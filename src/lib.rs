use std::convert::TryInto;
use std::io;

use apply::Also;
use async_trait::async_trait;
use camino::{FromPathBufError, Utf8Path, Utf8PathBuf};
use std::os::unix::fs::PermissionsExt;
use thiserror::Error;
use tokio::fs::File;
use tokio::io::AsyncWriteExt;
use Utf8PathError::*;

#[derive(Error, Debug)]
#[non_exhaustive]
pub enum Utf8PathError {
    #[error("could not write to file: {file:?}")]
    FileWriteError {
        source: io::Error,
        file: Utf8PathBuf,
    },

    #[error("could not create file: {file:?}")]
    FileCreateError {
        source: io::Error,
        file: Utf8PathBuf,
    },

    #[error("could not delete file: {file:?}")]
    FileDeleteError {
        source: io::Error,
        file: Utf8PathBuf,
    },

    #[error("could not read from file: {file:?}")]
    FileReadError {
        source: io::Error,
        file: Utf8PathBuf,
    },

    #[error("could not read metadata for file: {file:?}")]
    FileMetadataError {
        source: io::Error,
        file: Utf8PathBuf,
    },

    #[error("could not set permissions for file: {file:?}")]
    FilePermissionError {
        source: io::Error,
        file: Utf8PathBuf,
    },

    #[error("could not copy file: {file1:?} => {file2:?}")]
    FileCopyError {
        source: io::Error,
        file1: Utf8PathBuf,
        file2: Utf8PathBuf,
    },

    #[error("could read directory contents: {dir:?}")]
    ReadDirError { source: io::Error, dir: Utf8PathBuf },

    #[error("invalid filename: {dir:?} / {file:?}")]
    InvalidFilenameError { file: Utf8PathBuf, dir: Utf8PathBuf },

    #[error("could not convert filename to utf-8")]
    NonUtf8Error { source: FromPathBufError },
}

#[async_trait]
pub trait Utf8PathExt: AsRef<Utf8Path> {
    /// Changes the permissions found on a file or a directory.
    ///
    /// This is supported on Unix only.
    async fn set_permissions(&self, mode: u32) -> Result<(), Utf8PathError> {
        let path = self.as_ref();
        let mut permissions = tokio::fs::metadata(path)
            .await
            .map_err(|source| FileMetadataError {
                source,
                file: path.to_path_buf(),
            })?
            .permissions();
        permissions.set_mode(mode);
        tokio::fs::set_permissions(path, permissions)
            .await
            .map_err(|source| FilePermissionError {
                source,
                file: path.to_path_buf(),
            })
    }

    /// appends a file extension to a filename without replacing anything
    fn append_extension(&self, extension: &str) -> Utf8PathBuf {
        self.as_ref()
            .to_string()
            .also(|s| s.push('.'))
            .also(|s| s.push_str(extension))
            .into()
    }

    /// deletes the file
    async fn delete(&self) -> Result<(), Utf8PathError> {
        let file = self.as_ref();
        tokio::fs::remove_file(file)
            .await
            .map_err(|source| FileDeleteError {
                source,
                file: file.to_path_buf(),
            })
    }

    /// Read the entire contents of a file into a string.
    async fn read_file(&self) -> Result<String, Utf8PathError> {
        let path = self.as_ref();
        tokio::fs::read_to_string(path)
            .await
            .map_err(|source| FileReadError {
                source,
                file: path.to_path_buf(),
            })
    }

    /// returns a list of all files in the directory
    async fn directory_contents(&self) -> Result<Vec<Utf8PathBuf>, Utf8PathError> {
        let path = self.as_ref();
        let mut dir_entries = Vec::new();
        let mut files = tokio::fs::read_dir(path)
            .await
            .map_err(|source| ReadDirError {
                source,
                dir: path.to_path_buf(),
            })?;
        while let Ok(Some(entry)) = files.next_entry().await {
            let entry = entry
                .path()
                .try_into()
                .map_err(|source| NonUtf8Error { source })?;
            dir_entries.push(entry);
        }
        Ok(dir_entries)
    }

    /// write the data to a file
    async fn create_file(&self, data: &[u8]) -> Result<Utf8PathBuf, Utf8PathError> {
        let path = self.as_ref().to_path_buf();
        // write to file
        File::create(&path)
            .await
            .map_err(|source| FileCreateError {
                source,
                file: path.to_path_buf(),
            })?
            .write_all(data)
            .await
            .map_err(|source| FileWriteError {
                source,
                file: path.to_path_buf(),
            })?;

        // set file permissions to something reasonable
        self.set_permissions(0o666).await?;

        // return path to file
        Ok(path)
    }

    /// moves file to target directory
    async fn move_to(&self, directory: &Utf8Path) -> Result<Utf8PathBuf, Utf8PathError> {
        let old_file = self.as_ref().to_path_buf();
        // get path to new file
        let filename = old_file.file_name().ok_or_else(|| InvalidFilenameError {
            file: old_file.to_path_buf(),
            dir: directory.to_path_buf(),
        })?;
        let new_file = directory.join(filename);

        // nothing to do here? move along...
        if old_file == new_file {
            return Ok(old_file);
        }

        // try to use rename to move the file
        if tokio::fs::rename(&old_file, &new_file).await.is_ok() {
            return Ok(new_file);
        }

        // copy old file to new file
        tokio::fs::copy(&old_file, &new_file)
            .await
            .map_err(|source| FileCopyError {
                source,
                file1: old_file.clone(),
                file2: new_file.clone(),
            })?;

        old_file.delete().await?;

        Ok(new_file)
    }
}

impl<T: AsRef<Utf8Path>> Utf8PathExt for T {}
